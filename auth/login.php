<?php
require('../tools/mysql.php');
session_start();

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['password'])) {
    $conn   = connect();
    $result = mysqli_query($conn, "SELECT `value` FROM `settings` WHERE `key` = 'videochat_password'");

    if (!$result) {
        echo json_encode([
            'status'  => 'fail',
            'message' => 'Что-то пошло не так, повторите позднее.'
        ]);

        exit;
    }

    $row      = mysqli_fetch_assoc($result);
    $password = $row['value'] ?? null;

    if (password_verify($_POST['password'], $password)) {
        $_SESSION['authenticated'] = md5(time());

        echo json_encode([
            'status'  => 'ok',
            'message' => 'Добро пожаловать!'
        ]);

        exit;
    } else {
        echo json_encode([
            'status'  => 'fail',
            'message' => 'Неверный пароль, повторите еще раз.'
        ]);

        exit;
    }
}

echo json_encode([
    'status'  => 'fail',
    'message' => 'Что-то пошло не так.'
]);