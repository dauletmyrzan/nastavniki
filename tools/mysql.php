<?php
require('Dotenv.php');
use DevCoder\DotEnv;

(new DotEnv(__DIR__ . '/../.env'))->load();

function connect()
{
    $conn = mysqli_connect(getenv('MYSQL_HOST'), getenv('MYSQL_USERNAME'), getenv('MYSQL_PASSWORD'), getenv('MYSQL_DB'));
    mysqli_set_charset($conn, 'utf8mb4');

    if (mysqli_connect_error()) {
        echo mysqli_connect_error();
    }

    return $conn;
}