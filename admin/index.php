<?php
session_start();
if (!isset($_SESSION['adm_auth'])) {
    header('Location: /admin/login.php');
    exit;
}
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Панель администратора</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
    <style>
        .cursored {
            cursor: pointer;
            font-size: 15pt;
        }
        .cursored:hover {
            background-color: #eee;
        }
        .gallery {
            background-image: url('/img/gallery.png');
            background-position: 95% 15px;
            background-repeat: no-repeat;
            background-size: 100px;
        }
        .password {
            background-image: url('/img/password-icon.png');
            background-position: 95% 15px;
            background-repeat: no-repeat;
            background-size: 100px;
        }
    </style>
</head>
<body>
    <div class="container py-5">
        <h1>Панель администратора</h1>
        <div class="row mt-5 text-center">
            <div class="col-md-6">
                <div class="p-5 border cursored gallery" onclick="window.location.href='/admin/gallery.php'">
                    Галерея
                </div>
            </div>
            <div class="col-md-6">
                <div class="p-5 border cursored password" onclick="window.location.href='/admin/videochat.php'">
                    Пароль от видеочата
                </div>
            </div>
<!--            <div class="col-md-4">-->
<!--                <div class="p-5 border cursored" onclick="window.location.href='/admin/contacts.php'">-->
<!--                    Контакты-->
<!--                </div>-->
<!--            </div>-->
        </div>
    </div>
</body>
</html>