<?php
session_start();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    require('../tools/mysql.php');
    $conn     = connect();
    $login    = preg_replace('/[^a-zA-Z]/', '', $_POST['login']);
    $login    = mysqli_real_escape_string($conn, $login);
    $password = $_POST['password'];
    $result   = mysqli_query($conn, "SELECT * FROM `users` WHERE `login` = '$login'");

    if (!$result) {
        $fail = true;
    } else {
        $fail = false;
    }

    if (!$fail) {
        $row          = mysqli_fetch_assoc($result);
        $passwordHash = $row['password'] ?? '';

        if (password_verify($password, $passwordHash)) {
            $_SESSION['adm_auth'] = md5(time());
            header('Location: /admin');

            exit;
        } else {
            $fail = true;
        }
    }
}
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Видеочат - Панель администратора</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/admin.css">
</head>
<body style="background-color: #f4f4f4;">
<div class="container pt-5 text-center">
    <div class="row mb-5 justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body p-5 shadow">
                    <h3 class="mb-3">Вход в панель администратора</h3>
                    <?php
                    if (isset($fail) && $fail) {?>
                        <div class="alert alert-danger">Неверный логин или пароль</div>
                    <?php }?>
                    <form action="" enctype="multipart/form-data" method="post">
                        <div class="form-group mb-4">
                            <label for="login">Логин:</label>
                            <input type="text" name="login" id="login" class="form-control">
                        </div>
                        <div class="form-group mb-4">
                            <label for="password">Пароль:</label>
                            <input type="password" name="password" id="password" class="form-control">
                        </div>
                        <button class="btn btn-success btn-lg px-5">Войти</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="/js/admin.js"></script>
<script>
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>
</body>
</html>