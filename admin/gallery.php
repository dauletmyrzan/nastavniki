<?php
session_start();
if (!isset($_SESSION['adm_auth'])) {
    header('Location: /admin/login.php');
    exit;
}
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Галерея - Панель администратора</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/admin.css">
</head>
<body style="background-color: #f4f4f4;">
    <div class="container pt-5">
        <h1 class="mb-3">Галерея</h1>
        <a href="/admin" class="h5 mb-4 d-inline-block">Вернуться на главную</a>
        <div class="row align-items-center justify-content-center mb-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body p-4 shadow">
                        <form action="upload.php" enctype="multipart/form-data" method="post">
                            <div class="form-group mb-4">
                                <label for="files" class="h3">Загрузите файлы</label><br>
                                <input type="file" name="file" id="file">
                            </div>
                            <button class="btn btn-success btn-lg">Загрузить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <?php
            $gallery = glob(__DIR__ . '/../img/gallery/*.*');
            natsort($gallery);

            foreach ($gallery as $photo) {
                $split = explode('/', $photo);
                $photo = end($split);
                ?>
                <div class="col-md-3 mb-4 text-center">
                    <div class="gallery-item" style="background-image:url('/img/gallery/<?php echo $photo?>')"></div>
                    <div class="text-muted my-3"><?php echo $photo?></div>
                    <button class="btn btn-success btn-sm mt-2 px-4 mx-auto d-block rename" data-name="<?php echo $photo?>">Переименовать</button>
                    <button class="btn btn-danger btn-sm mt-2 px-4 mx-auto d-block remove" data-name="<?php echo $photo?>">Удалить</button>
                </div>
            <?php }?>
        </div>
    </div>
    <script src="/js/vendor/jquery.min.js"></script>
    <script src="/js/admin.js"></script>
</body>
</html>