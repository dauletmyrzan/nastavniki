<?php
define('ROOT', __DIR__ . '/../');

function renderHtml($message) {
    echo "<h3>$message</h3>";
    echo "<a href='/admin'>Вернуться в панель администратора</a>";
    exit;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != 0) {
        renderHtml($_FILES["files"]["error"]);
    }

    $allowed  = ["jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png"];
    $filename = $_FILES["file"]["name"];
    $filetype = $_FILES["file"]["type"];
    $filesize = $_FILES["file"]["size"];

    // Verify file extension
    $ext = pathinfo($filename, PATHINFO_EXTENSION);

    if (!array_key_exists($ext, $allowed)) {
        renderHtml('Недопустимое формат фото. Допущены jpg, png, gif.');
    }

    // Verify file size - 10MB maximum
    $maxsize = 10 * 1024 * 1024;

    if($filesize > $maxsize) {
        renderHtml('Допустимый размер файла - 10Мб.');
    }

    // Verify MIME type of the file
    if (in_array($filetype, $allowed)) {
        // Check whether file exists before uploading it
        if (file_exists(ROOT . "img/gallery/" . $filename)) {
            renderHtml('Файл с таким названием уже существует.');
        } else {
            move_uploaded_file($_FILES["file"]["tmp_name"], ROOT . "img/gallery/" . $filename);

            renderHtml('Фото успешно загружено!');
        }
    } else {
        renderHtml('Произошла ошибка. Повторите позднее.');
    }
}