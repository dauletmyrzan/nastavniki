<?php
session_start();
if (!isset($_SESSION['adm_auth'])) {
    header('Location: /admin/login.php');
    exit;
}

$matchFail = null;
$success = null;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $password   = $_POST['password'];
    $passwordRe = $_POST['password_re'];
    $matchFail  = ($password !== $passwordRe);
    $success    = false;

    if (!$matchFail) {
        require('../tools/mysql.php');
        $conn  = connect();
        $hash  = password_hash($password, PASSWORD_DEFAULT);
        $query = "UPDATE `settings` SET `value` = '" . $hash . "' WHERE `key` = 'videochat_password'";

        if (mysqli_query($conn, $query)) {
            $success = true;
        }
    }
}
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Видеочат - Панель администратора</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/admin.css">
</head>
<body style="background-color: #f4f4f4;">
    <div class="container pt-5">
        <h1 class="mb-3">Видеочат</h1>
        <a href="/admin" class="h5 mb-4 d-inline-block">Вернуться на главную</a>
        <div class="row mb-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body p-5 shadow text-center">
                        <div class="row justify-content-center">
                            <div class="col-md-6">
                                <h3>Смена пароля</h3>
                                <div class="text-muted h6 mb-3">Здесь вы можете сменить пароль для входа в видео-комнаты.</div>
                                <?php
                                if (isset($matchFail) && $matchFail) {?>
                                    <div class="alert alert-danger">Пароли не совпадают</div>
                                <?php } else if (isset($success) && $success) {?>
                                    <div class="alert alert-success">Пароль успешно изменен!</div>
                                <?php } else if (isset($success) && !$success) {?>
                                    <div class="alert alert-danger">Возникла неожиданная ошибка!</div>
                                <?php }?>
                                <form action="" enctype="multipart/form-data" method="post">
                                    <div class="form-group mb-4">
                                        <label for="password">Новый пароль:</label>
                                        <input type="password" name="password" id="password" class="form-control">
                                    </div>
                                    <div class="form-group mb-4">
                                        <label for="password_re">Повторите новый пароль:</label>
                                        <input type="password" name="password_re" id="password_re" class="form-control">
                                    </div>
                                    <button class="btn btn-success btn-lg">Изменить пароль</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="/js/admin.js"></script>
    <script>
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
</body>
</html>