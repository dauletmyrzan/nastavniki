<?php
define('ROOT', __DIR__ . '/../');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $name    = $_POST['name'];
    $newName = $_POST['new_name'];

    if (rename(ROOT . "img/gallery/" . $name, ROOT . "img/gallery/" . $newName)) {
        echo json_encode([
            'status' => 'ok',
        ]);

        exit;
    }
}

echo json_encode([
    'status' => 'fail',
]);