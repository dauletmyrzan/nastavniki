<?php

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $name = $_POST['name'];

    if (unlink(__DIR__ . '/../img/gallery/' . $name)) {
        echo json_encode([
            'status' => 'ok',
        ]);

        exit;
    }
}

echo json_encode([
    'status' => 'fail',
]);
