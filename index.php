<?php
session_start();
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Проект "Наставники"</title>

    <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/photoswipe.css">
    <link rel="stylesheet" href="css/default-skin/default-skin.css">
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/media.css?v=1.9">
    <link rel="stylesheet" href="css/app.css?v=1.9">
</head>
<body>
    <div class="container-fluid px-0" id="content_wrapper">
        <div class="section" id="initialLoader">
            <div class="bg-part bg-part-1"></div>
            <div class="bg-part bg-part-2"></div>
            <div class="bg-part bg-part-3"></div>
            <div class="bg-part bg-part-4"></div>
            <div id="start_logo"></div>
            <div id="loading"></div>
        </div>
        <div id="sectionLoader" style="top: 0;">
            <div class="section" id="alphabet" style="top: 0">
                <header class="py-2">
                    <div class="row text-center mx-0 align-items-center">
                        <div class="column-4">
                            <img src="img/logo_kz.webp" alt="" width="120">
                        </div>
                        <div class="column-4">
                            <img src="img/main_logo.webp" alt="" width="150">
                        </div>
                        <div class="column-4">
                            <img src="img/logo_ru.webp" alt="" width="120">
                        </div>
                    </div>
                </header>
                <div id="alphabet_wrapper">
                    <div class="row mx-0">
                        <div class="col-md-3">
                            <div class="alphabet-letter alphabet-letter-a woosh">
                                <div class="cover"></div>
                                <div class="gridCover1">
                                    <div class="gridLabel">Игры</div>
                                    <div class="gridLabelUnderline"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="alphabet-letter alphabet-letter-b"></div>
                        </div>
                        <div class="col-md-3">
                            <?php if (isset($_SESSION['authenticated'])) {?>
                                <a href="videochat" onclick="setTimeout(function(){window.location.href='videochat';},2000)">
                                    <div class="alphabet-letter alphabet-letter-c woosh">
                                        <div class="cover"></div>
                                        <div class="gridCover1">
                                            <div class="gridLabel">Видеочат</div>
                                            <div class="gridLabelUnderline"></div>
                                        </div>
                                    </div>
                                </a>
                            <?php } else {?>
                                <div class="alphabet-letter alphabet-letter-c woosh">
                                    <div class="cover"></div>
                                    <div class="gridCover1">
                                        <div class="gridLabel">Видеочат</div>
                                        <div class="gridLabelUnderline"></div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col-md-3">
                            <div class="alphabet-letter alphabet-letter-d"></div>
                        </div>
                        <div class="col-md-3">
                            <div class="alphabet-letter alphabet-letter-e"></div>
                        </div>
                        <div class="col-md-3">
                            <div class="alphabet-letter alphabet-letter-f woosh">
                                <div class="cover"></div>
                                <div class="gridCover1">
                                    <div class="gridLabel">Фотогалерея</div>
                                    <div class="gridLabelUnderline"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="alphabet-letter alphabet-letter-g"></div>
                        </div>
                        <div class="col-md-3">
                            <div class="alphabet-letter alphabet-letter-h woosh">
                                <div class="cover"></div>
                                <div class="gridCover1">
                                    <div class="gridLabel">Контакты</div>
                                    <div class="gridLabelUnderline"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section" id="games" style="top: 881px">
                <div class="menuButton"></div>
                <div class="section-image"></div>
                <div class="section-info">
                    <div class="letterTriangle" style="left: 288px;">
                        <div class="letterTriangleRotatedMask">
                            <div class="letterTriangleTexture">
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="letterBottomContent">
                            <div class="letterTitle">Игры</div>
                            <div class="letterUnderline"></div>
                        </div>
                        <div class="row justify-content-center text-center">
                            <div class="col-md-4">
                                <a href="http://www.crazygames.ru/igra/connect-4" target="_blank">
                                    <div class="connect-4"></div>
                                    <div class="game-label">Connect 4</div>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a href="https://www.crazygames.ru/igra/drawaria-online" target="_blank">
                                    <div class="drawar"></div>
                                    <div class="game-label">Drawaria.online</div>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a href="http://ru.battleship-game.org/id53745782" target="_blank">
                                    <div class="seawar"></div>
                                    <div class="game-label">Морской бой</div>
                                </a>
                            </div>
                        </div>
                        <!-- Modal -->
                        <div id="iframe_holder" class="modal fade" tabindex="-1" aria-hidden="true">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Морской бой</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <iframe src="" id="pacman" frameborder="0"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section" id="videochat" style="top: 1762px;">
                <div class="menuButton"></div>
                <div class="container h-100">
                    <div class="row justify-content-center text-center align-items-center h-100">
                        <div class="col-md-6">
                            <form action="auth/login.php" method="post">
                                <img src="img/videochat.png" width="200" alt="">
                                <div class="form-group mt-5">
                                    <label for="password">Введите пароль для входа:</label>
                                    <input type="password" name="password" id="password">
                                </div>
                                <button class="my-btn">Войти</button>
                                <div class="alert alert-danger mt-3" style="display:none;">Неверный пароль, повторите еще раз.</div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section" id="gallery" style="top: 2643px">
                <div class="menuButton"></div>
                <header class="py-2">
                    <div class="row text-center mx-0 align-items-center">
                        <div class="column-4">
                            <img src="img/logo_kz.webp" alt="" width="120">
                        </div>
                        <div class="column-4">
                            <img src="img/main_logo.webp" alt="" width="150">
                        </div>
                        <div class="column-4">
                            <img src="img/logo_ru.webp" alt="" width="120">
                        </div>
                    </div>
                </header>
                <div id="slick">
                    <?php
                    $i = 0;
                    $gallery = glob(__DIR__ . '/img/gallery/*.*');
                    natsort($gallery);

                    $galleryChunks = array_chunk($gallery, 8);

                    foreach ($galleryChunks as $row) {?>
                        <div class="row mx-auto">
                            <?php
                            foreach ($row as $photo) {
                                list($width, $height) = getimagesize($photo);
                                $split = explode('/', $photo);
                                $photo = end($split);
                                ?>
                                <div class="col-md-3">
                                    <div class="gallery-item" data-index="<?php echo $i?>" data-width="<?php echo $width?>" data-height="<?php echo $height?>" data-src="img/gallery/<?php echo $photo?>">
                                        <div class="cover"></div>
                                        <img data-lazy="img/gallery/<?php echo $photo?>" data-width="<?php echo $width?>" width="100%" data-height="<?php echo $height?>">
                                    </div>
                                </div>
                                <?php
                                $i++;
                            } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <div class="section" id="contacts" style="top: 3524px">
                <div class="menuButton"></div>
                <div class="section-image"></div>
                <div class="container-fluid mt-5" style="max-width: 75%;">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="h5">
                                Официальный сайт Фонда "Дара" <a href="https://www.dara.kz" target="_blank">www.dara.kz</a>
                            </div>
                            <div class="h5">Контакты</div>
                            <div>
                                <div class="mb-3"><b>Адрес:</b> Казахстан, город Нур-Султан, проспект Мангилик Ел, дом 37/1, н.п. 4, почтовый индекс Z05H9B0</div>
                                <div class="mb-3"><b>Телефон:</b> +7 705 114 32 50</div>
                                <div class="mb-3"><b>Email:</b> info@dara.kz</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="wrapper">
        <div id="colorWipe">
            <div id="colorWipe1" style="background: rgb(253 211 124);"></div>
            <div id="colorWipe2" style="background: rgb(239 155 82);"></div>
            <div id="colorWipe3" style="background: rgb(208 92 64);"></div>
        </div>
    </div>

    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="pswp__bg"></div>
        <div class="pswp__scroll-wrap">
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>
            <div class="pswp__ui pswp__ui--hidden">
                <div class="pswp__top-bar">
                    <div class="pswp__counter"></div>
                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                    <button class="pswp__button pswp__button--share" title="Share"></button>
                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>
                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                </button>
                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                </button>
                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>
            </div>
        </div>
    </div>

    <script src="js/vendor/jquery.min.js"></script>
    <script src="js/vendor/popper.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/vendor/jquery-ui.min.js"></script>
    <script src="js/vendor/jquery.easing.min.js"></script>
    <script src="js/vendor/photoswipe.js"></script>
    <script src="js/vendor/photoswipe-ui-default.js"></script>
    <script src="js/vendor/jquery.bez.js"></script>
    <script src="js/vendor/slick.min.js"></script>
    <script src="js/app.js?v=1.9"></script>
</body>
</html>