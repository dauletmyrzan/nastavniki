$(document).ready(function () {
    $(".remove").click(function () {
        let dis = $(this);
        let name = $(this).data('name');

        if (confirm('Вы уверены, что хотите удалить фото?')) {
            $.ajax({
                url: '/admin/remove.php',
                type: 'post',
                data: {
                    name: name,
                },
                success (response) {
                    if (response['status'] === 'ok') {
                        dis.remove();
                    } else {
                        window.location.reload();
                    }
                }
            });
        }
    });

    $(".rename").on("click", function () {
        let name = $(this).data("name");

        let newName = prompt("Введите новое название", name);

        while (newName.length === 0) {
            newName = prompt("Введите новое название", name);
        }

        $.ajax({
            url: '/admin/rename.php',
            type: 'post',
            data: {
                name: name,
                new_name: newName,
            },
            dataType: 'json',
            success(response) {
                if (response['status'] === 'ok') {
                    window.location.reload();
                } else {
                    alert('Возникла ошибка, попробуйте позднее');
                }
            }
        });
    });
});