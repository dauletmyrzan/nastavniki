$(document).ready(function () {
    let vh = $(window).height();
    let wooshAudio, pagesAudio, popAudio;

    let isMobile = function () {
        return (/Android|webOS|iPhone|iPad|Mac|Macintosh|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));
    };

    $(window).on('beforeunload', function () {
        window.scrollTo(0, 0);
    });

    window.scrollTo(0, 0);

    let timer1, timer2, timer3, timer4, timer5, timer6;
    let menu = $(".menuButton");
    let animationSpeed = 600;

    let colorWipe = function () {
        let colorWipe1 = $("#colorWipe1");
        let colorWipe2 = $("#colorWipe2");
        let colorWipe3 = $("#colorWipe3");
        colorWipe1.animate({
            'top': 0,
        }, animationSpeed, $.bez([0.65, 0, 0.35, 1]));

        clearTimeout(timer1);
        clearTimeout(timer2);
        clearTimeout(timer3);
        clearTimeout(timer4);
        clearTimeout(timer5);
        clearTimeout(timer6);

        timer1 = setTimeout(function () {
            colorWipe1.animate({
                'height': 0,
            }, animationSpeed, $.bez([0.37, 0, 0.63, 1]));
        }, animationSpeed);

        timer2 = setTimeout(function () {
            pagesAudio.play();
            colorWipe2.animate({
                'top': 0,
            }, animationSpeed, $.bez([0.65, 0, 0.35, 1]));

            timer3 = setTimeout(function () {
                colorWipe2.animate({
                    'height': 0,
                }, animationSpeed, $.bez([0.37, 0, 0.63, 1]));
            }, animationSpeed);

            timer4 = setTimeout(function () {
                colorWipe3.animate({
                    'top': 0,
                }, animationSpeed, $.bez([0.37, 0, 0.63, 1]));

                timer5 = setTimeout(function () {
                    colorWipe3.animate({
                        'height': 0,
                    }, animationSpeed, $.bez([0.37, 0, 0.63, 1]));

                    timer6 = setTimeout(function () {
                        $("#colorWipe").find("div").css({
                            'height': '',
                            'top': '',
                        });
                    }, 1050);
                }, 300);
            }, 200);
        }, 100);
    }

    let setHeights = function () {
        let divs = $(".alphabet-letter, .gallery-item");
        let width = ($(window).outerHeight() - 142.78)/2;

        divs.css({
            'height': width + "px"
        });
    };

    $(window).resize(function () {
        setHeights();
    });

    let showLoader = function (delay) {
        setTimeout(function (delay) {
            $("#loading").animate({
                'top': '60%',
                'opacity': 1
            }, 1000);
        }, delay);
    };

    let setSection = function (name, delay = 1000) {
        if (isMobile()) {
            if (name !== 'gallery') {
                colorWipe();
            }
        }  else {
            colorWipe();
        }

        setTimeout(function () {
            if (!isMobile()) {
                let top = getSectionTopByName(name);

                $("#sectionLoader").css({
                    'top': '-' + top + 'px'
                });
                if (name === 'alphabet') {
                    window.scrollTo(0, 0);
                }
            } else {
                $(".section").hide();
                $("#" + name).show();
                $('html, body').animate({
                    scrollTop: 0
                }, name === 'gallery' ? 1000 : 10);
            }
        }, isMobile() && name === 'gallery' ? 10 : 1000);
    };

    function change(state) {
        if(state === null) { // initial page
            setSection("alphabet");
        } else {
            setSection(state.section);
        }
    }

    $(window).on("popstate", function(e) {
        change(e.originalEvent.state);
    });

    (function(original) { // overwrite history.pushState so that it also calls
        // the change function when called
        history.pushState = function(state) {
            change(state);
            return original.apply(this, arguments);
        };
    })(history.pushState);

    $(".alphabet-letter").on("click", function () {
        if ($(this).hasClass('alphabet-letter-a')) {
            history.pushState({ section: "games" }, null, "/games");
            setSection('games');
        } else if ($(this).hasClass('alphabet-letter-c')) {
            history.pushState({ section: "videochat" }, null, "/videochat");
            setSection('videochat');
        } else if ($(this).hasClass('alphabet-letter-f')) {
            history.pushState({ section: "gallery" }, null, "/gallery");
            setSection('gallery');
        } else if ($(this).hasClass('alphabet-letter-h')) {
            history.pushState({ section: "contacts" }, null, "/contacts");
            setSection('contacts');
        }
    });

    let getCurrentSection = function () {
        let scrollTop = $(window).scrollTop();

        if (scrollTop === 0) {
            return 'alphabet';
        } else if (scrollTop === vh) {
            return 'games';
        } else if (scrollTop === vh * 2) {
            return 'videochat';
        } else if (scrollTop === vh * 3) {
            return 'gallery';
        } else if (scrollTop === vh * 4) {
            return 'contacts';
        }
    };

    let getSectionTopByName = function (name) {
        if (name === 'alphabet') {
            return 0;
        } else if (name === 'games') {
            return vh;
        } else if (name === 'videochat') {
            return vh * 2;
        } else if (name === 'gallery') {
            return vh * 3;
        } else if (name === 'contacts') {
            return vh * 4;
        }
    };

    menu.on('click', function () {
        setSection('alphabet');
    });

    let init = function () {
        showLoader(500);

        setTimeout(function () {
            $("#initialLoader").css({
                'display': 'none'
            });
        }, 8000);

        if (getCurrentSection() === 'alphabet') {
            setTimeout(function () {
                setSection('alphabet');
            }, 7000);
        }

        setHeights();

        setSectionTops();

        wooshAudio = document.createElement('audio');
        wooshAudio.setAttribute('src', '/audio/woosh.mp3');
        pagesAudio = document.createElement('audio');
        pagesAudio.setAttribute('src', '/audio/pages_2.mp3');
        popAudio = document.createElement('audio');
        popAudio.setAttribute('src', '/audio/pop.mp3');

        if (!isMobile()) {
            $("#slick").slick({
                slide: '.row',
                infinite: false,
                lazyLoad: 'ondemand',
                init: function () {
                    fitSlickImages();
                },
                lazyLoaded: function () {
                    fitSlickImages();
                }
            });
        } else {
            loadSlickImagesMobile();
        }

        fitSlickImages();
    };

    // Init empty gallery array
    let container = [];

    // Loop over gallery items and push it to the array
    $('#gallery').find('.gallery-item').each(function() {
        let item = {
            src: $(this).data('src'),
            w: $(this).data('width'),
            h: $(this).data('height'),
        };
        container.push(item);
    });

    // Define click event on gallery item
    $('.gallery-item').click(function (event) {
        popAudio.play();
        // Prevent location change
        event.preventDefault();

        // Define object and gallery options
        let $pswp = $('.pswp')[0],
            options = {
                index: $(this).data('index'),
                bgOpacity: 0.8,
                captionEl: false,
                tapToClose: true,
                shareEl: false,
                fullscreenEl: false,
            };

        let gallery = new PhotoSwipe($pswp, PhotoSwipeUI_Default, container, options);
        gallery.init();
    });

    $("#videochat").on("submit", "form", function (e) {
        $.ajax({
            url: '/auth/login.php',
            type: 'post',
            data: {
                password: $("#password").val()
            },
            dataType: 'json',
            success (response) {
                if (response['status'] === 'ok') {
                    window.location.href = "/videochat";
                } else {
                    $("#videochat").find(".alert-danger").show();
                }
            }
        });
        e.preventDefault();

        return false;
    });

    let setSectionTops = function () {
        $("#games").css({
            'top': vh + 'px'
        });
        $("#videochat").css({
            'top': vh*2 + 'px'
        });
        $("#gallery").css({
            'top': vh*3 + 'px'
        });
        $("#contacts").css({
            'top': vh*4 + 'px'
        });
    };

    $(".woosh").on("mouseenter", function () {
        wooshAudio.play();
    }).on("mouseleave", function () {
        wooshAudio.pause();
        wooshAudio.currentTime = 0;
    });

    let fitSlickImages = function () {
        $(".slick-slide").find("img").each(function () {
            let img = $(this);

            if (img.data('height') > img.data('width')) {
                img.css({
                    'width': '100%',
                    'height': 'auto'
                });
            } else {
                img.css({
                    'width': 'auto',
                    'height': '100%'
                });
            }
        });
    }

    let loadSlickImagesMobile = function () {
        $(".gallery-item").find("img").each(function () {
            let img = $(this);
            let src = img.data('lazy');
            img.attr('src', src);

            if (img.data('height') > img.data('width')) {
                img.css({
                    'width': '100%',
                    'height': 'auto'
                });
            } else {
                img.css({
                    'width': 'auto',
                    'height': '100%'
                });
            }
        });
    }

    init();
});