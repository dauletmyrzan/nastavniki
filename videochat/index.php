<?php
session_start();
if (!isset($_SESSION['authenticated'])) {
    header('Location: /');
    exit;
}
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Проект "Наставники"</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../css/app.css?v=1.6">
    <link rel="stylesheet" href="../css/media.css?v=1.6">
</head>
<body>
<div class="container-fluid px-0" id="content_wrapper">
    <div class="section" id="rooms" style="top: 0px;">
        <div class="menuButton" onclick="window.location.href='/'" style="z-index: 100;"></div>
        <div id="main_room">
            <a href="https://dara.whereby.com/all" target="_blank" class="title">Общая комната</a>
            <div class="room-layer"></div>
        </div>
        <div id="other_rooms" class="mt-5">
            <div class="row mx-0 justify-content-center">
                <div class="col-md-2 main-room-hidden">
                    <a href="https://dara.whereby.com/all" target="_blank" class="pp-link">
                        <div class="room room-0"></div>
                        <div class="room-name">Общая комната</div>
                    </a>
                </div>
                <div class="col-md-2">
                    <a href="https://dara.whereby.com/safari" target="_blank" class="pp-link">
                        <div class="room room-1"></div>
                        <div class="room-name">Сафари</div>
                    </a>
                </div>
                <div class="col-md-2">
                    <a href="https://dara.whereby.com/island" target="_blank" class="pp-link">
                        <div class="room room-2"></div>
                        <div class="room-name">Остров приключений</div>
                    </a>
                </div>
                <div class="col-md-2">
                    <a href="https://dara.whereby.com/magic" target="_blank" class="pp-link">
                        <div class="room room-3"></div>
                        <div class="room-name">Волшебная страна</div>
                    </a>
                </div>
                <div class="col-md-2">
                    <a href="https://dara.whereby.com/jungle" target="_blank" class="pp-link">
                        <div class="room room-4"></div>
                        <div class="room-name">Джунгли</div>
                    </a>
                </div>
                <div class="col-md-2">
                    <a href="https://dara.whereby.com/tropic" target="_blank" class="pp-link">
                        <div class="room room-5"></div>
                        <div class="room-name">Тропики</div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/js/vendor/jquery.min.js"></script>
<script src="/js/vendor/popper.min.js"></script>
<script src="/js/vendor/bootstrap.min.js"></script>
<script src="/js/vendor/jquery-ui.min.js"></script>
<script src="/js/vendor/jquery.easing.min.js"></script>
</body>
</html>